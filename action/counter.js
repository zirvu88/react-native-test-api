export const increaseCounter = (data) => { //function yang dipanggil ketika Text Increase di klik
	return {
		type: "INCREASE_COUNTER", //value type
		payload: data
	}
}
export const decreaseCounter = (data) => { //function yang dipanggil ketika Text Decrease di klik
	return {
		type: "DECREASE_COUNTER", //value type
		payload: data
	}
}