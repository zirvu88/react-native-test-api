import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Button
} from "react-native";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

//kalau mau pake var Counter App yg di share masukkan functionnya dari action
import { increaseCounter,decreaseCounter } from '../action/counter';
class CounterApp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ownvar : false, // variable sendiri
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', width: 200, justifyContent: 'space-around' }}>
                    <TouchableOpacity onPress={() => this.props.decreaseCounter()}>
                        <Text style={{ fontSize: 20 }}>Decrease</Text>
                    </TouchableOpacity>
                    <Text style={{ fontSize: 20 }}>{this.props.counter.total}</Text>
                    <TouchableOpacity onPress={() => this.props.increaseCounter()}>
                        <Text style={{ fontSize: 20 }}>Increase</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', width: 200, justifyContent: 'space-around' }}>
                    <Button title={'Back'} onPress={() => 
                        this.props.navigation.navigate("MainMenu") //cara akses var sendiri
                    }/>
                </View>
            </View>
        );
    }
}

//cara panggil function yg ada di action this.props.decreaseCounter()
//cara akses var dari reducers this.props.counter.total
//cara navigation this.props.navigation.navigate("nama yang ada di AppNavigator")

function mapStateToProps(state) {
    return {
        counter: state.counter //untuk mengambil semua var di redux, di redux
    }
}

function mapDispatchToProps(dispatch) {
    return {
        increaseCounter: bindActionCreators(increaseCounter, dispatch), //untuk menjalankan function yang ada di action
        decreaseCounter: bindActionCreators(decreaseCounter, dispatch) //untuk menjalankan function yang ada di action
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CounterApp)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});