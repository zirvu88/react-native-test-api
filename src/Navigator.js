import { createStackNavigator } from 'react-navigation'
import { 
	fromLeft,
	fromTop,
	fadeIn,
	zoomIn,
	zoomOut,
	flipY,
	flipX,
} from 'react-navigation-transitions';
import CounterApp from './CounterApp' //file yang mau di koneksikan
import NextPage from './nextpage'
import Login from './login'
import MainMenu from './mainmenu'
import WebView from './webview'

export default createStackNavigator(
  {
    CounterApp: {
      screen: CounterApp,
      navigationOptions: ({navigation}) => ({
        header: null
      })
    },
    NextPage: {
      screen: NextPage,
      navigationOptions: ({navigation}) => ({
        header: null
      })
    },
    Login: {
      screen: Login,
      navigationOptions: ({navigation}) => ({
        header: null
      })
    },
    MainMenu: {
      screen: MainMenu,
      navigationOptions: ({navigation}) => ({
        header: null
      })
    },
    WebView: {
      screen: WebView,
      navigationOptions: ({navigation}) => ({
        header: null
      })
    }
  },
  {
    initialRouteName: 'Login', //screen mana yg mau di jalankan terlebih dahulu
    transitionConfig: () => fromLeft(),
  }
);
