import React, { Component } from "react"
import { Animated, Button, View } from 'react-native'
import { Text } from 'native-base'
import {
    StyleSheet,
    TouchableOpacity,
    AppRegistry
} from "react-native"
import{
    Root
} from "native-base"
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

//kalau mau pake var Counter App yg di share masukkan functionnya dari action
import { increaseCounter,decreaseCounter } from '../action/counter'; 

class nextpage extends Component {
    render() {
        return (
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <Text>Nilai dari redux : {this.props.counter.total}</Text>
                <Button full title={"Go to CounterApp"} onPress={() => this.props.navigation.navigate('CounterApp')}/>
            </View>
        )
    }
}

//cara akses var dari reducers this.props.counter.total
//cara navigation this.props.navigation.navigate("nama yang ada di AppNavigator")

function mapStateToProps(state) {
    console.log(state)
    return {
        counter: state.counter //untuk mengambil semua var di redux, di redux
    }
}

export default connect(mapStateToProps)(nextpage)
