import React, {Component} from 'react'
import {
	Container,Header,Title,
	Content,Footer,FooterTab,
	Button,Left,Right,
	Body,Icon,Text,
	Accordion,View,StackNavigator,
	ActionSheet,Root,Badge,
	Card,CardItem,Thumbnail,
	CheckBox,ListItem,DatePicker,
  	DeckSwiper,Fab,Input,Item,Form,
  	Label,List
} from 'native-base'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ScrollView,StyleSheet,Image,Alert } from 'react-native'
import { cek } from '../action/mainmenu';

import SideMenu from 'react-native-side-menu'
const uri = 'https://pickaface.net/gallery/avatar/Opi51c74d0125fd4.png';


class mainmenu extends Component{

	constructor(props) {
		super(props);
		this.state = {
		}
	}

	// componentWillMount() {
	// 	if(!this.props.cekmain.loaded){
	// 		this.props.checking()
	// 	}
	// }

	nextpage = ()=>{
		this.props.navigation.navigate('WebView')
	}
	render(){
		//const menu untuk menu side
		const menu = (
			<ScrollView scrollsToTop={false} style={{
			    flex: 1,
			    width: window.width,
			    height: window.height,
			    backgroundColor: 'gray',
			    padding: 20,
			}}>
		     	<View style={{marginBottom: 20,marginTop: 20,}}>
		    		<Image style={{width: 48,height: 48,borderRadius: 24,flex: 1,}} source={{ uri }}/>
		        	<Text style={{position: 'absolute',left: 70,top: 20,}}>Arief</Text>
		      	</View>
		      	<Button full 
					    onPress={() => alert("Nama: Arief\nApp: Testing")}
			        	style={{paddingTop: 10}}>
			      	<Text>About</Text>
			    </Button>
		      	<Button full 
					    onPress={() => 
					    	Alert.alert(
					    		'Konfirmasi',
					    		'Apakah Anda Ingin Keluar?',
					    		[
					    			{text: "No", onPress: ()=> alert("Ok :)")},
					    			{text: "Yes", onPress: ()=>this.props.navigation.navigate("Login")},
					    		],
  								{ cancelable: false }
					    	)
					    }
			        	style={{paddingTop: 10}}>
			      	<Text>Logout</Text>
			    </Button>
		    </ScrollView>
		)
		return(
      		<SideMenu menu={menu}>
				<Container>
		        <Header>
		        	<Left>
            			<Button transparent>
              				<Icon name='menu' />
            			</Button>
          			</Left>
	        		<Body>
	        			<Title>Main Menu</Title> 
	        		</Body>
	        		<Right/>
		        </Header>
			        <View style={{
						flex: 1,
		        		alignItems: 'center',
		        		justifyContent: 'center'
					}}>
						<View>
							<Text uppercase={false}>Slide ke samping untuk logout >></Text>
						</View>
						<View style={{paddingTop: 10}}/>
						<View>
							<Button onPress={()=>this.nextpage()}>
								<Text uppercase={false}>Web View</Text>
							</Button>
						</View>
					</View>
	      		</Container>
      		</SideMenu>
		)
	}
}

function mapStateToProps(state) {
    return {
        cekmain: state.cekmain
    }
}

function mapDispatchToProps(dispatch) {
    return {
        checking: bindActionCreators(cek, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(mainmenu)