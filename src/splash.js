import React, {Component} from 'react'
import {
	View,Text,Image,
	Animated
} from 'react-native'
// import Spinner from 'react-native-loading-spinner-overlay'
// import Ajax from '../helper/ajax'

// import {tambahSetting} from '../action/tambah-setting'


import { createTransition, FlipX, SlideLeft } from 'react-native-transition'
const Transition = createTransition(SlideLeft)
import Navigator from './Navigator'


import { createSwitchNavigator } from 'react-navigation'
import Login from './login'

var image = "../logo.jpg"

class Splash extends Component{
	componentDidMount(){
		let a = setTimeout(()=>{
			this.nextpage()
			clearInterval(a)
		},3000)
	}
	nextpage = () => {
		// this.props.navigation.navigate("Login")
	    Transition.show(
	      	<Navigator></Navigator>
	    )
	}
	render(){
		return(
      		<Transition>
				<Image style={{flex: 1, width: '100%', height: '100%'}} source={require(image)} />
      		</Transition>
		)
	}
}
export default Splash