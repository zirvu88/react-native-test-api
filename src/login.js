import React, {Component} from 'react'
import {
	View,Image,Animated,
	StyleSheet,ActivityIndicator
} from 'react-native'
import {
	Text,Button,Input,
	Container,Content,Form,
	Item,Label,Header,
	Icon
} from 'native-base';
import { createTransition, SlideLeft } from 'react-native-transition'
const Transition = createTransition(SlideLeft)

// import { createSwitchNavigator } from 'react-navigation'
// import MainMenu from './mainmenu'

// const Menu = createSwitchNavigator({ MainMenu: MainMenu })

class login extends Component{

	constructor(props) {
		super(props);
		this.state = {
			username : '',
			password : '',
			logging : false
		}
	}
	//Sumbit data form Login
	dologin = ()=>{
		var request = {
			username: this.state.username,
			password: this.state.password
		}
		//post api cek username sama password
		fetch('http://pay.caberawit.club/index_1.php', { //url
			method: 'POST',
		  	headers: {
		    	'Content-Type': 'application/json',
		  	},
		  	body: JSON.stringify(request) //body
		}).then((response)=>{
 			return response.json();
		}).then((data)=>{ //response data
	    	this.setState({logging: false}, function(){
	    		if(data.response){ //jika benar username sama password maka slide ke main menu
				    this.props.navigation.navigate("MainMenu")
	    		}else{
	    			alert("Username dan Password salah\n\nUsername: Arief\nPassword: 123")
	    		}
	    	})	
		})
	}

	render(){
		return(
			<Transition>
				<View style={styles.container}>
					<View style={styles.loginContainer}>
						<Image source={require('../logo.jpg')} resizeMode = "contain" style={styles.logo} />
						<Text style={{ color : 'black', fontWeight : 'bold', fontSize : 20}}>
							Test Login
						</Text>
					</View>
					<View  style={{flex : 1, justifyContent : 'center'}}>
						{(this.state.logging) ? <ActivityIndicator size="large" color="#0000ff"/> : null}
						<Form style={{ flex : 1, justifyContent : 'center'}}>
							<Item style={{ marginRight : 15}}>
								<Input 
									placeholder="Username" 
									onChangeText={(val) => this.setState({username: val})}
								/>
							</Item>
							<Item style={{ marginRight : 15}}>
								<Input 
									placeholder="Password" 
									secureTextEntry={true} 
									onChangeText={(val) => this.setState({password: val})}
								/>
							</Item>
							<Button full primary 
								style={styles.button} 
								onPress={() => {
									this.setState({logging: true}),
									this.dologin()
								}}>
								<Text style={{ color : 'white', fontWeight : 'bold'}}>
									Login
								</Text>
							</Button>
						</Form>
					</View>
				</View>
			</Transition>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	loginContainer:{
		alignItems: 'center',
		flexGrow: 1,
		justifyContent: 'center'
	},
	logo: {
		width: 200,
		height: 200
	},
	button:{
		marginTop : 20, 
		marginLeft : 15, 
		marginRight : 15
	}
});

export default login