import { createSwitchNavigator } from 'react-navigation'
import { 
	fromLeft,
	fromTop,
	fadeIn,
	zoomIn,
	zoomOut,
	flipY,
	flipX,
} from 'react-navigation-transitions';
import CounterApp from './src/CounterApp' //file yang mau di koneksikan
import NextPage from './src/nextpage'
import Splash from './src/splash'
import Login from './src/login'
import MainMenu from './src/mainmenu'

export default createSwitchNavigator(
  {
    Splash: Splash,
  },
  {
    initialRouteName: 'Splash', //screen mana yg mau di jalankan terlebih dahulu
    transitionConfig: () => flipY(),
  }
);
