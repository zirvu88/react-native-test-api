//jika mau tambah redux tambah di sini

import {combineReducers} from 'redux'
import counterReducers from './reducer-counter' //file redux
import cekmain from './reducer-main' //file redux



const allReducers = combineReducers({
	counter: counterReducers, //combine kan di sini, jadi jika mau akses varnya, state.counter
	cekmain: cekmain,
})

export default allReducers