const initialState = {
    total: 0 //var yang di share
}

export default function(state = initialState, action){
	//perubahan terhadap nilai dari var yang di share harus di dalam file itu sendiri
    switch (action.type) { //value type yang di tekan, jika yang di klik tadi Increase maka value total bertambah
        case 'INCREASE_COUNTER':
            return { total: state.total + 1 }
        case 'DECREASE_COUNTER':
            return { total: state.total - 1 }
    }
    return state;
}