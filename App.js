import React, { Component } from "react"
import {
    StyleSheet,
    TouchableOpacity,
    AppRegistry
} from "react-native"
import{
    Root
} from "native-base"
import { Provider } from 'react-redux'
import AppNavigator from './AppNavigator'
import { createStackNavigator } from 'react-navigation'
AppRegistry.registerComponent('test_api', () => App)

import store from './store'

class App extends Component {
    render() {
        return (
            <Root>
                <Provider store={store}>
                    <AppNavigator />
                </Provider>
            </Root>
        )
    }
}

export default App
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
